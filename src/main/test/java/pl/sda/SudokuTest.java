package pl.sda;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.sda.exception.NumberRepeatedInColumnException;
import pl.sda.exception.NumberRepeatedInRectException;
import pl.sda.exception.NumberRepeatedInRowException;

@RunWith(JUnitParamsRunner.class)
public class SudokuTest {


    //TODO: 4 testy
    // - poprawne sudoku
    // - exception z wierszami
    // - exception z kolumnami
    // - exception z kwadratami

    @Test
    @Parameters(method = "correctSudoku")
    public void givenCorrectSudokuWhenSolveThenNoException(int[][] sudoku) {
        Sudoku.checkSudoku(sudoku);
    }

    @Test(expected = NumberRepeatedInRowException.class)
    @Parameters(method = "incorrectRowSudoku")
    public void givenRepeatedNumberInRowWhenSolveThenException(int[][] sudoku) {
        Sudoku.checkSudoku(sudoku);
    }

    @Test(expected = NumberRepeatedInColumnException.class)
    @Parameters(method = "incorrectColumnSudoku")
    public void givenRepeatedNumberInColumnWhenSolveThenException(int[][] sudoku) {
        Sudoku.checkSudoku(sudoku);
    }

    @Test(expected = NumberRepeatedInRectException.class)
    @Parameters(method = "incorrectRectSudoku")
    public void givenRepeatedNumberInRectWhenSolveThenException(int[][] sudoku) {
        Sudoku.checkSudoku(sudoku);
    }



    public int[][][] correctSudoku() {
        return new int[][][]{
                {
                        {5, 3, 4, 6, 7, 8, 9, 1, 2},
                        {6, 7, 2, 1, 9, 5, 3, 4, 8},
                        {1, 9, 8, 3, 4, 2, 5, 6, 7},
                        {8, 5, 9, 7, 6, 1, 4, 2, 3},
                        {4, 2, 6, 8, 5, 3, 7, 9, 1},
                        {7, 1, 3, 9, 2, 4, 8, 5, 6},
                        {9, 6, 1, 5, 3, 7, 2, 8, 4},
                        {2, 8, 7, 4, 1, 9, 6, 3, 5},
                        {3, 4, 5, 2, 8, 6, 1, 7, 9}
                },
        };
    }

    public int[][][] incorrectRowSudoku() {
        return new int[][][]{
                {
                        {5, 1, 4, 6, 7, 8, 9, 1, 2},
                        {6, 7, 2, 1, 9, 5, 3, 4, 8},
                        {1, 9, 8, 3, 4, 2, 5, 6, 7},
                        {8, 5, 9, 7, 6, 1, 4, 2, 3},
                        {4, 2, 6, 8, 5, 3, 7, 9, 1},
                        {7, 1, 3, 9, 2, 4, 8, 5, 6},
                        {9, 6, 1, 5, 3, 7, 2, 8, 4},
                        {2, 8, 7, 4, 1, 9, 6, 3, 5},
                        {3, 4, 5, 2, 8, 6, 1, 7, 9}
                },
        };
    }

    public int[][][] incorrectRectSudoku() {
        return new int[][][]{
                {
                        {1, 1, 3, 6, 7, 8, 9, 1, 2},
                        {6, 7, 2, 1, 9, 5, 3, 4, 8},
                        {1, 9, 8, 3, 4, 2, 5, 6, 7},
                        {8, 5, 9, 7, 6, 1, 4, 2, 3},
                        {4, 2, 6, 8, 5, 3, 7, 9, 1},
                        {7, 1, 3, 9, 2, 4, 8, 5, 6},
                        {9, 6, 1, 5, 3, 7, 2, 8, 4},
                        {2, 8, 7, 4, 1, 9, 6, 3, 5},
                        {3, 4, 5, 2, 8, 6, 1, 7, 9}
                },
        };
    }

    public int[][][] incorrectColumnSudoku() {
        return new int[][][]{
                {
                        {5, 3, 4, 6, 7, 8, 9, 1, 2},
                        {6, 7, 2, 1, 9, 5, 3, 4, 8},
                        {1, 9, 8, 3, 4, 2, 5, 6, 7},
                        {8, 5, 9, 7, 6, 1, 4, 2, 3},
                        {4, 2, 6, 8, 5, 3, 7, 9, 1},
                        {7, 1, 3, 9, 2, 4, 8, 5, 6},
                        {6, 6, 1, 5, 3, 7, 2, 8, 4},
                        {2, 8, 7, 4, 1, 9, 6, 3, 5},
                        {3, 4, 5, 2, 8, 6, 1, 7, 9}
                },
        };
    }

}