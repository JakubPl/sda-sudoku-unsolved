package pl.sda;

import pl.sda.exception.NumberRepeatedInColumnException;
import pl.sda.exception.NumberRepeatedInRectException;
import pl.sda.exception.NumberRepeatedInRowException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Sudoku {

    public static final int SUDOKU_SIZE = 9;

    public static void main(String[] args) {
        int[][] numbers = {
                {5, 3, 4, 6, 7, 8, 9, 1, 2},
                {6, 7, 2, 1, 9, 5, 3, 4, 8},
                {1, 9, 8, 3, 4, 2, 5, 6, 7},
                {8, 5, 9, 7, 6, 1, 4, 2, 3},
                {4, 2, 6, 8, 5, 3, 7, 9, 1},
                {7, 1, 3, 9, 2, 4, 8, 5, 6},
                {9, 6, 1, 5, 3, 7, 2, 8, 4},
                {2, 8, 7, 4, 1, 9, 6, 3, 5},
                {3, 4, 5, 2, 8, 6, 1, 7, 9}};

        //Set columnNumbers = new HashSet<>();
        checkSudoku(numbers);
    }

    public static void checkSudoku(int[][] numbers) {
        List<HashSet<Integer>> uniqueColumnNumbers = new ArrayList<>();
        List<HashSet<Integer>> uniqueRectNumbers = new ArrayList<>();
        for (int i = 0; i < SUDOKU_SIZE; i++) {
            uniqueColumnNumbers.add(new HashSet<>());
            uniqueRectNumbers.add(new HashSet<>());
        }

        for (int rowNumber = 0; rowNumber < SUDOKU_SIZE; rowNumber++) {
            int[] row = numbers[rowNumber];
            HashSet<Integer> uniqueRowNumbers = new HashSet<>();
            for (int columnNumber = 0; columnNumber < SUDOKU_SIZE; columnNumber++) {
                int value = row[columnNumber];
                int rectNumber = (columnNumber / 3) + (rowNumber / 3) * 3;

                boolean wasAddedInRect = uniqueRectNumbers.get(rectNumber).add(value);
                if (!wasAddedInRect) {
                    throw new NumberRepeatedInRectException();
                }
                boolean wasAddedInColumn = uniqueColumnNumbers.get(columnNumber).add(value);
                if (!wasAddedInColumn) {
                    throw new NumberRepeatedInColumnException();
                }
                boolean wasAddedInRow = uniqueRowNumbers.add(value);
                if (!wasAddedInRow) {
                    throw new NumberRepeatedInRowException();
                }
                System.out.print(value);
            }
            System.out.println();
        }

        System.out.println();
    }
}
